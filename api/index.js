// pacotes e modulos
const http = require('http')
const URL = require('url')
const fs = require('fs')
const path = require('path')
const data = require('./url.json')

function writeFile(cb){
    fs.writeFile(
        path.join(__dirname, "url.json"),
        JSON.stringify(data, null, 2),
        err => {
            if(err) throw err
            cb(JSON.stringify({message: "OK"}))
        }
    )
}


//create a server
http.createServer((req, res) =>{
    const{name, url} = URL.parse(req.url, true).query

    res.writeHead(200, {
        'Access-Control-Allow-Origin': '*'
    })

    // listagem
    if(!name || !url)
    return res.end(JSON.stringify(data))

    // deletar
    if(del){
    data.urls = data.urls.filter(item => String(item.url) !== String(url))

    return  writeFile((msg) => {
        res.end(msg)
    })
    
    }
    //criar 
    data.urls.push({name, url})
    return writeFile((msg) => res.end(msg))
}).listen(3000, () => console.log('Server is running'));